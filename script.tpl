#!/bin/bash
sleep 5m
sudo su - root

# Install AWS EFS Utilities
yum update -y
yum install -y httpd php
yum install -y amazon-efs-utils
service httpd start

# Mount EFS
efs_id="${efs_id}"
mount -t efs $efs_id:/ /var/www/html
echo "<?php  print phpinfo(); ?>" > /var/www/html/index.php     
# Edit fstab so EFS automatically loads on reboot
echo $efs_id:/ /var/www/html efs defaults,_netdev 0 0 >> /etc/fstab