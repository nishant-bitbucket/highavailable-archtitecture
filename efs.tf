resource "aws_security_group" "efs" {
  name        = "efs-mnt"
  description = "Allows NFS traffic from instances within the VPC."
  vpc_id      = "${aws_vpc.demo_vpc.id}"

  ingress {
    from_port = 2049
    to_port   = 2049
    protocol  = "tcp"

    cidr_blocks = [
      "${aws_vpc.demo_vpc.cidr_block}",
    ]
  }

  egress {
    from_port = 2049
    to_port   = 2049
    protocol  = "tcp"

    cidr_blocks = [
      "${aws_vpc.demo_vpc.cidr_block}",
    ]
  }

  tags = {
    Name = "allow_nfs-ec2"
  }
}


resource "aws_efs_file_system" "main" {
  tags = {
    Name = "demo-vpc-efs"
  }
}

# Creates a mount target of EFS in a specified subnet
# such that our instances can connect to it.
resource "aws_efs_mount_target" "app_a" {

  file_system_id = "${aws_efs_file_system.main.id}"
  subnet_id      = "${aws_subnet.app_subnet_a.id}"

  security_groups = [
    "${aws_security_group.efs.id}",
  ]
}

resource "aws_efs_mount_target" "app_b" {

  file_system_id = "${aws_efs_file_system.main.id}"
  subnet_id      = "${aws_subnet.app_subnet_b.id}"

  security_groups = [
    "${aws_security_group.efs.id}",
  ]
}
