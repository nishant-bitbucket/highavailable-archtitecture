## Creating Web Servers for app subnet A

resource "aws_security_group" "sg_webserver" {

  name   = "sg_webserver"
  vpc_id = "${aws_vpc.demo_vpc.id}"
  tags = {
    Name        = "sg-webserver"
    Environment = "${var.environment}"
  }

}

resource "aws_security_group_rule" "allow_all" {
  type              = "ingress"
  cidr_blocks       = ["10.1.0.0/24"]
  to_port           = 0
  from_port         = 0
  protocol          = "-1"
  security_group_id = "${aws_security_group.sg_webserver.id}"
}

resource "aws_security_group_rule" "outbound_allow_all" {
  type = "egress"

  cidr_blocks       = ["0.0.0.0/0"]
  to_port           = 0
  from_port         = 0
  protocol          = "-1"
  security_group_id = "${aws_security_group.sg_webserver.id}"
}

data "template_file" "script_init_webservers" {
  template = "${file("script.tpl")}"
  vars = {
    efs_id = "${aws_efs_file_system.main.id}"
  }

}



resource "aws_instance" "webserver_a" {
  ami                    = "ami-0a887e401f7654935"
  instance_type          = "t2.micro"
  subnet_id              = "${aws_subnet.app_subnet_a.id}"
  vpc_security_group_ids = ["${aws_security_group.sg_webserver.id}"]
  key_name               = "${aws_key_pair.myec2key.key_name}"
  user_data              = "${data.template_file.script_init_webservers.rendered}"

  tags = {
    Name        = "nishant-webserver-a"
    Environment = "${var.environment}"
  }

}

##  END Creating Web Servers for app subnet A


## Creating Web Servers for app subnet B

data "template_file" "script_mount" {
  template = "${file("script-mount.tpl")}"
  vars = {
    efs_id = "${aws_efs_file_system.main.id}"
  }

}

resource "aws_instance" "webserver_b" {
  ami                    = "ami-0a887e401f7654935"
  instance_type          = "t2.micro"
  subnet_id              = "${aws_subnet.app_subnet_b.id}"
  vpc_security_group_ids = ["${aws_security_group.sg_webserver.id}"]
  key_name               = "${aws_key_pair.myec2key.key_name}"
  user_data              = "${data.template_file.script_mount.rendered}"

  tags = {
    Name        = "nishant-webserver-b"
    Environment = "${var.environment}"
  }

}
## END Creating Web Servers for app subnet B
